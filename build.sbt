import sbt._
import Keys._

name := "quill-pg-test"

lazy val commonSettings = Seq(
  organization := "com.softwaremill",
  version := "1.0-SNAPSHOT",
  scalaVersion := "2.12.4"
)

lazy val quillPgTest = (project in file("."))
  .settings(commonSettings)
  .aggregate(quill)

lazy val common = (project in file("common"))
  .settings(commonSettings)
  .settings(
    libraryDependencies ++= Seq(
      "org.postgresql" % "postgresql" % "42.2.5",
      "org.flywaydb" % "flyway-core" % "4.1.2")
  )

val circeVersion = "0.10.1"

lazy val quill = (project in file("quill"))
  .settings(commonSettings)
  .settings(
    libraryDependencies ++= Seq(
      "ch.qos.logback" % "logback-classic" % "1.2.3",
      "io.getquill" %% "quill-async-postgres" % "3.0.0",
      "io.getquill" %% "quill-jdbc" % "3.0.0",
      "io.circe" %% "circe-core" %   circeVersion,
      "io.circe" %% "circe-generic" % circeVersion,
      "io.circe" %% "circe-parser" %  circeVersion,
    )
  )
  .dependsOn(common)

lazy val bucket = (project in file("bucket"))
  .settings(commonSettings)
  .settings(
    libraryDependencies ++= Seq(
      "ch.qos.logback" % "logback-classic" % "1.2.3",
      "io.getquill" %% "quill-async-postgres" % "3.0.0",
      "io.getquill" %% "quill-jdbc" % "3.0.0",
      "io.circe" %% "circe-core" %   circeVersion,
      "io.circe" %% "circe-generic" % circeVersion,
      "io.circe" %% "circe-parser" %  circeVersion,
    )
  )
  .dependsOn(common)