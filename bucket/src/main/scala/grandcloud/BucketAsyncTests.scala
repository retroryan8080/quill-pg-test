package grandcloud

import java.util.{Date, UUID}

import com.github.mauricio.async.db.RowData
import io.circe.{Encoder, Json}
import io.circe.parser.parse
import io.getquill.context.async.{AsyncContext, SqlTypes}
import io.getquill._
import io.getquill.context.async.SqlTypes.SqlTypes

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.concurrent.ExecutionContext.Implicits.global

object BucketAsyncTests extends App    {

  lazy val ctx = new PostgresAsyncContext(SnakeCase, "ctx")

  import ctx._

  implicit val prototypeEncoder = MappedEncoding[Json, String](_.toString())
  implicit val prototypeDecoder = MappedEncoding[String, Json](parse(_).fold(error => throw error, jsValue => jsValue))


 def insertWithGeneratedId(): Future[Unit] = {

    val rawJson: String =
      """
      {
        "foo2": "bar2",
        "baz2": 1232,
        "list of stuff": [ 4, 5, 6 ]
      }
      """

    val parseResult =  parse(rawJson).fold(error => throw error, jsValue => jsValue)

    val bucket = Bucket(BucketId(UUID.randomUUID().toString), "Test Json Stuff", parseResult, new Date(), None)

    val q = quote {
      query[Bucket].insert(lift(bucket))
    }

    ctx.run(q).map { r =>
      println(s"Inserted, generated id: $r")
      println()
    }
  }

  def selectAll(): Future[Unit] = {
    val q = quote {
      query[Bucket]
    }

    val value: Future[List[Bucket]] = ctx.run(q)

    logResults("All Bucket", value)
  }

  private def logResults[R](label: String, f: Future[Seq[R]]): Future[Unit] = {
    f.map { r =>
      println(label)
      r.foreach(println)
      println()
    }
  }

  val tests = for {
    _ <- insertWithGeneratedId()
    _ <- selectAll()
  } yield ()

  try Await.result(tests, Duration.Inf)
  finally ctx.close()

}
