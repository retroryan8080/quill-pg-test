package grandcloud

import java.util.{Date, UUID}

import io.circe.Json
import io.getquill.{PostgresJdbcContext, SnakeCase}

import  io.circe.parser._


trait JSONEncoding {
  val ctx: PostgresJdbcContext[SnakeCase.type] // your context should go here

  import ctx._

  implicit val jsonDecoder: Decoder[Json] =
    decoder((index, row) => {

      //JSONObject
      val string = stringDecoder.apply(index-1, row)
      println(s"index : $index")
      println(s"row : $row")
      println(s"PRE PARSE : $string")

      parse(string).fold(error => throw error, jsValue => jsValue)
    })

  implicit val jsonEncoder: Encoder[Json] =
    encoder(java.sql.Types.OTHER, (index, value, row) => {
      println(s"index : $index")
      println(s"row : $row")

      val valueStr: String = value.noSpaces
      row.setObject(index, valueStr, java.sql.Types.OTHER)
    })

}


object BucketSyncTests extends JSONEncoding {

  lazy val ctx: PostgresJdbcContext[SnakeCase.type] = new PostgresJdbcContext(SnakeCase, "syncctx")

  import ctx._

  def insertWithGeneratedId() = {

    val rawJson: String =
      """
      {
        "foo": "bar",
        "baz": 123,
        "list of stuff": [ 4, 5, 6 ]
      }
      """

    val parseResult =  parse(rawJson).fold(error => throw error, jsValue => jsValue)

    val bucket = Bucket(BucketId(UUID.randomUUID().toString), "Test Json Stuff", parseResult, new Date(), None)

    val q = quote {
      query[Bucket].insert(lift(bucket))
    }

    val value = ctx.run(q)

    println(s"Inserted, generated id: ${value}")
    println()

  }

  def selectAll(): Unit = {
    val q = quote {
      query[Bucket]
    }
    val value: List[Bucket] = ctx.run(q)
    logResults("All Bucket", value)
  }

  def logResults[R](label: String, f: Seq[R]) = {
    println(label)
    f.foreach(println)
  }

  insertWithGeneratedId()
  selectAll()
  ctx.close()

}
