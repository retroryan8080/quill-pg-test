package grandcloud

import java.util.Date

import io.circe.Decoder.Result

import scala.concurrent.Future
import io.circe._
import io.circe.generic.semiauto._


trait BucketDAO {

  def lookup(id: String): Future[List[Bucket]]

  def all(pageSize: Int): Future[Seq[Bucket]]

  def update(bucket: Bucket): Future[Int]

  def delete(id: String): Future[Int]

  def create(bucket: Bucket): Future[Int]

  def close(): Future[Unit]
}


case class BucketId(id: String) extends AnyVal
case class Bucket(id: BucketId, description: String, someJson:Json, createdAt: Date, updatedAt: Option[Date])


case class MultiBuckets(buckets: Seq[Bucket])


object Bucket {

  implicit val bucketIdDecoder: Decoder[BucketId] = deriveDecoder[BucketId]
  implicit val bucketIdEncoder: Encoder[BucketId] = deriveEncoder[BucketId]

  implicit val bucketDecoder: Decoder[Bucket] = deriveDecoder[Bucket]
  implicit val bucketEncoder: Encoder[Bucket] = deriveEncoder[Bucket]

  implicit val DateFormat : Encoder[Date] with Decoder[Date] = new Encoder[Date] with Decoder[Date] {
    override def apply(a: Date): Json = Encoder.encodeLong.apply(a.getTime)
    override def apply(c: HCursor): Result[Date] = Decoder.decodeLong.map(s => new Date(s)).apply(c)
  }
}


