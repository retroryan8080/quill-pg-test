package com.softwaremill.sql

import org.flywaydb.core.Flyway

trait DbSetup {
  val connectionString = "jdbc:postgresql://localhost/pg_test"

  def dbSetup(): Unit = {
    val flyway = new Flyway()
    flyway.setDataSource(connectionString, "pgtest", "pgtest")
    //flyway.clean()
    flyway.migrate()
  }
}