Copied from https://github.com/softwaremill/scala-sql-compare

## Postgres Setup:

```
    psql postgres
    CREATE ROLE pgtest WITH LOGIN PASSWORD 'pgtest';
    CREATE DATABASE pg_test;
    GRANT ALL PRIVILEGES ON DATABASE pg_test TO pgtest;
    \q
    psql -U pgtest pg_test
```

## Flyway Schema Setup:

```
  sbt quill/run
```

## Postgres Testing:

```
    sbt bucket/run
```


## Challenges:

1 - [The Quill Decoder](https://github.com/getquill/quill/blob/eec9643a00a1726d133f18b4789b9d91d4c1854a/quill-jdbc/src/main/scala/io/getquill/context/jdbc/Decoders.scala#L16) offsets the index to be +1 so inside the bucket sync decoder I have to -1 the row index? Is that correct?

2 - How do you create custom async encoder and decoders? I took a pass at it in `grandcloud.BucketAsyncTests`. If you uncomment the trait `grandcloud.AsyncCirceJsonExtensions` and try to compile it gives the error:

```
[error] /typesafe/quill-pg-test/bucket/src/main/scala/grandcloud/BucketAsyncTests.scala:34:9: ambiguous implicit values:
[error]  both value decoder in trait AsyncCirceJsonExtensions of type => AsyncCirceJsonExtensions.this.Decoder[io.circe.Json]
[error]  and macro method anyValDecoder in trait LowPriorityImplicits of type [T <: AnyVal]=> AsyncCirceJsonExtensions.this.Decoder[T]
[error]  match expected type AsyncCirceJsonExtensions.this.BaseDecoder[io.circe.Json]
[error]     new Decoder[Json](SqlTypes.CHAR) {
[error]         ^
[error] one error found
```

And I am not sure which SqlTypes I should use? I would have guessed SqlTypes.OTHERS but that doesn't seem to be a type?

3 - In the file `com.softwaremill.sql.QuillTests.selectMetroSystemsWithMostLines` if you uncomment the line with:

`MetroSystemWithLineCount(msName, cName, aggregated.size)`

It gives the error:

```
[error] java.lang.IllegalStateException: The monad composition can't be expressed using applicative joins. Faulty expression: '(x14, x15, x17).size'. Free variables: 'List(x14, x15, x17)'.
```
